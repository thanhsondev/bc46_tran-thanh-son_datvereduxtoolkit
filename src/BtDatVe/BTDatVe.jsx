import React from "react";
import data from "./data.json";
import ChairList from "./ChairList";
import "./style.scss";
import Result from "./Result";
const BTDatVe = () => {
  return (
    <div className=" BTDatVe ">
      <div className="container">
        <div className="row pt-5">
          <div className="col-8 ">
            <h1 className="text-warning text-center">Đặt vé xem phim</h1>
            {/* <h5 className="text-light bg-info text-center">Màn hình</h5> */}
            <div className="screen text-center">Màn hình</div>
            <ChairList data={data} />
          </div>
          <div className="col-4">
            <h3 className="text-light">Danh sách ghế bạn chọn</h3>
            <Result></Result>

          </div>
        </div>
      </div>
    </div>
  );
};

export default BTDatVe;
