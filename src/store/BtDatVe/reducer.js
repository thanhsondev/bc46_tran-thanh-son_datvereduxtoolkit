import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    chairBookings: [],
    chairBookeds: [],
}

const BtDatVeSlice = createSlice({
  name: 'BtdatVe',
  initialState,
  reducers: {
    bookingAction: (state, action)=> {
        // const data = {...state.chairBookings}
        const index = state.chairBookings.findIndex(e => e.soGhe === action.payload.soGhe)
        if (index === -1) {
            state.chairBookings.push(action.payload)
    
        } else {
          state.chairBookings.splice(index, 1)
          
        }
    },
    bookedAction: (state, {payload}) => {
      state.chairBookeds = [...state.chairBookeds, ...state.chairBookings]
      state.chairBookings = []

    }
  }
});

export const {actions: BTDatVeActions, reducer: BTDatVeReducer} = BtDatVeSlice

