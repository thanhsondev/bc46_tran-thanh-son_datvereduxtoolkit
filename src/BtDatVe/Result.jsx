import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { BTDatVeActions } from "../store/BtDatVe/reducer";

const Result = () => {
  const { chairBookings } = useSelector(state =>state.btDatVe);
  const dispatch = useDispatch()
  return (
    <div className="d-flex flex-column">
      <button className="btn btn-warning w-full">Ghế đã đặt</button>
      <button className="btn btn-success my-2">Ghế đang chọn</button>
      <button className="btn btn-light mb-2">Ghế trống</button>
      <table className="table text-white text-center" border={2}>
        <thead>
          <th>Ghế</th>
          <th>Giá</th>
          <th>Thao tác</th>
        </thead>
        <tbody className="text-warning">
          {chairBookings.map((ghe) => {
            return (
              <tr>
                <td>{ghe.soGhe}</td>
                <td>{ghe.gia}</td>
                <td>
                  <button className="btn btn-danger" onClick={()=>dispatch(BTDatVeActions.bookingAction(ghe))}>Hủy</button>
                </td>
              </tr>

            );
          })}
          <tr>
            <td>Tổng tiền</td>
            <td>{chairBookings.reduce((total, ghe)=> (total += ghe.gia), 0)}</td>
          </tr>
        </tbody>
      </table>
      <div className="text-center">
        <button className="btn btn-success"
          onClick={()=>{dispatch(BTDatVeActions.bookedAction())}}
        >Thanh toán</button>
      </div>
    </div>
  );
};

export default Result;
