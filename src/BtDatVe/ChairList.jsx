import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  return (
    <div className="">
      {data.map((hangGhe, index) => {

        // console.log('index: ', index);
       
        return (
          <div className="d-flex " >
            <div className="text-warning hangGhe " >{hangGhe.hang}</div>
            <div className="mb-1 ">
              {hangGhe.danhSachGhe.map((ghe) => {
                if(index === 0) {
                  return <span className="rowNumber text-warning " >{ghe.soGhe}</span>
                } else {

                  return <Chair ghe={ghe} key={ghe.soGhe}></Chair>;
                }
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ChairList;
