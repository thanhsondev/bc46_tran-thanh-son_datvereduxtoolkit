import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BTDatVeActions } from '../store/BtDatVe/reducer'
import cn from 'classnames'

const Chair = ({ghe}) => {
  const dispatch = useDispatch()
  const {chairBookings, chairBookeds} = useSelector(state => state.btDatVe)
  console.log('chairBookeds: ', chairBookeds);

    return (
             
              <button className= {cn('ghe Chair', {
                booking: chairBookings.find((i) => i.soGhe === ghe.soGhe),
                booked: chairBookeds.find((i) => i.soGhe === ghe.soGhe),
                
              })}
              onClick={()=> {
                dispatch(BTDatVeActions.bookingAction(ghe))
              }}
              >{ghe.soGhe}</button>
    )

}

export default Chair
